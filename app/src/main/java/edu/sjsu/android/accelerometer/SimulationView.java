package edu.sjsu.android.accelerometer;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.view.WindowManager;
import android.widget.ImageView;
//import android.content.Context.SENSOR_SERVICE;
import android.graphics.Point;

public class SimulationView extends View implements SensorEventListener {

    private SensorManager sm;
    private Sensor sensor;
    private Display mDisplay;

    private Bitmap mField;
    private Bitmap mBasket;
    private Bitmap mBitmap;
    private static final int BALL_SIZE = 150;
    private static final int BASKET_SIZE = 180;

    private float mXOrigin;
    private float mYOrigin;
    private float mHorizontalBound;
    private float mVerticalBound;

    private Particle mBall;
    private float mSensorX;
    private float mSensorY;
    private float mSensorZ;
    private long mSensorTimeStamp;

    public SimulationView(Context context) {
        super(context);

        mBall = new Particle();

        // Initialize images from drawable
        Bitmap ball = BitmapFactory.decodeResource(getResources(), R.drawable.ball);
        mBitmap = Bitmap.createScaledBitmap(ball, BALL_SIZE, BALL_SIZE, true);

        Bitmap basket = BitmapFactory.decodeResource(getResources(), R.drawable.basket);
        mBasket = Bitmap.createScaledBitmap(basket, BASKET_SIZE, BASKET_SIZE, true);

        Options opts = new Options();
        opts.inPreferredConfig = Bitmap.Config.RGB_565;
        mField = BitmapFactory.decodeResource(getResources(), R.drawable.field, opts);

        WindowManager mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        mDisplay = mWindowManager.getDefaultDisplay();

        mHorizontalBound = (float)mDisplay.getWidth() - 50;
        mVerticalBound = (float)mDisplay.getHeight() - 50;


        // Callbacks for SensorEventListener?

        sm = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sensor = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);



    }

    public void onSizeChanged(){
        //super(context);
        // Initialize images from drawable
        Bitmap ball = BitmapFactory.decodeResource(getResources(), R.drawable.ball);
        mBitmap = Bitmap.createScaledBitmap(ball, BALL_SIZE, BALL_SIZE, true);
        Bitmap basket = BitmapFactory.decodeResource(getResources(), R.drawable.basket);
        mBasket = Bitmap.createScaledBitmap(basket, BASKET_SIZE, BASKET_SIZE, true);
        Options opts = new Options();
        opts.inPreferredConfig = Bitmap.Config.RGB_565;
        mField = BitmapFactory.decodeResource(getResources(), R.drawable.field, opts);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy){

    }


    public void startSimulation(){
        sm.registerListener(this,
                sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void stopSimulation(){
        sm.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event){
        if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            mSensorX = event.values[0];
            mSensorY = event.values[1];
            mSensorZ = event.values[2];
            mSensorTimeStamp = event.timestamp;


            if (mDisplay.getRotation() == Surface.ROTATION_0){
                mXOrigin = (float)Math.pow(event.values[0], 2);
                mYOrigin = (float)Math.pow(event.values[1], 2);

            }else if (mDisplay.getRotation() == Surface.ROTATION_90){
                mXOrigin = (float)Math.pow(event.values[1], 2);
                mYOrigin = (float)Math.pow(event.values[0], 2);
            }
            else if (mDisplay.getRotation() == Surface.ROTATION_180) {
                mXOrigin = (float) Math.pow(event.values[1], 2);
                mYOrigin = (float) Math.pow(event.values[2], 2);
             }
            else if (mDisplay.getRotation() == Surface.ROTATION_270){
                mXOrigin = (float)Math.pow(event.values[2], 2);
                mYOrigin = (float)Math.pow(event.values[1], 2);
            }

        }
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawBitmap(mField, 0, 0, null);
        canvas.drawBitmap(mBasket, 470, 500, null);

        mBall.updatePosition(mSensorX, mSensorY, mSensorZ, mSensorTimeStamp);
        mBall.resolveCollisionWithBounds(mHorizontalBound, mVerticalBound);

        canvas.drawBitmap(mBitmap,
                (mXOrigin - BALL_SIZE / 2) + mBall.mPosX,
                (mYOrigin - BALL_SIZE / 2) - mBall.mPosY, null);
        invalidate();
    }
}
